
setup() {
	read -e -p "Disk to be formatted and used by this setup [e.g. /dev/sda]: " DISK_DEVICE

	while true; do
	    read -s -p "LUKS Passphrase for /: " LUKS_PASS
	    echo
	    read -s -p "LUKS Passphrase for / (verify): " LUKS_PASS_2
	    echo
	    [ "$LUKS_PASS" = "$LUKS_PASS_2" ] && break || echo "Passwords do not match."
	done

	while true; do
        read -s -p "Ansible Vault Passphrase: " ANSIBLE_VAULT_PASS
        echo
        read -s -p "Ansible Vault Passphrase (verify): " ANSIBLE_VAULT_PASS_2
        echo
        [ "$ANSIBLE_VAULT_PASS" = "$ANSIBLE_VAULT_PASS_2" ] && break || echo "Passwords do not match."
    done


	read -e -p "Hostname: " NEW_HOSTNAME
	hostnamectl set-hostname "$NEW_HOSTNAME"

	while true; do
	    read -p "Install drivers for nvidia graphics card [y/n]: " yn
	    case $yn in
		[Yy]* ) ANSIBLE_NVIDIA="y"; break;;
		[Nn]* ) ANSIBLE_NVIDIA="n"; break;;
		* ) echo "Please answer yes or no.";;
	    esac
	done
	
	if [ "$ANSIBLE_NVIDIA" == "y" ]; then
		while true; do
		    read -p "Is this an OPTIMUS laptop and should xorg be adjusted accordingly [y/n]: " yn
		    case $yn in
			[Yy]* ) ANSIBLE_NVIDIA_OPTIMUS="y"; break;;
			[Nn]* ) ANSIBLE_NVIDIA_OPTIMUS="n"; break;;
			* ) echo "Please answer yes or no.";;
		    esac
		done
	fi

    if [ "$ANSIBLE_NVIDIA_OPTIMUS" == "y" ]; then
        lspci
        read -e -p "Nvidia BusID (e.g. 02:00.00) in Xorg format [e.g. 2:0:0]: " ANSIBLE_NVIDIA_BUSID
    fi

	# Partitioning
	parted -s "$DISK_DEVICE" \
		mklabel gpt \
		mkpart primary ext2 1 1024M \
		mkpart primary fat32 1024M 2048M \
		mkpart primary ext4 2048M 100% \
		set 1 boot on \
		set 2 esp on  \
		set 3 LVM on


	# Encrypt {{ disk_device_crypt_part_number }}. partition on $DISK_DEVICE 
	# See "Partitioning" section for more info about this very partition.
	echo -en "$LUKS_PASS" | cryptsetup -c aes-xts-plain -s 512 luksFormat "$DISK_DEVICE"3
	echo -en "$LUKS_PASS" | cryptsetup luksOpen "$DISK_DEVICE"3 luks

	# Setup LVM on the just encrypted 
	pvcreate /dev/mapper/luks
	vgcreate vg0 /dev/mapper/luks
	lvcreate --size 8G vg0 --name swap
	lvcreate -l +100%FREE vg0 --name root

	# Make fs on lvm parts
	mkfs.ext4 /dev/mapper/vg0-root
	mkswap /dev/mapper/vg0-swap

	# and for boot? idk, needed that last time in vm
	mkfs.ext2 "$DISK_DEVICE"1
	mkfs.fat -F 32 "$DISK_DEVICE"2

	# Mount partitions
	mount /dev/mapper/vg0-root /mnt
	mkdir /mnt/boot
	mkdir /mnt/efi
	mount "$DISK_DEVICE"1 /mnt/boot
	mount "$DISK_DEVICE"2 /mnt/efi
	swapon /dev/mapper/vg0-swap

	# Install base system
	pacstrap /mnt base base-devel linux linux-firmware openssh git vim grub ansible efibootmgr

	# fstab gen
	genfstab -pU /mnt >> /mnt/etc/fstab

	echo "tmpfs	/tmp	tmpfs	defaults,noatime,mode=1777	0	0" >> /mnt/etc/fstab

	#echo 'Chrooting into installed system to continue setup...'
    	mkdir /mnt/setup
	cp "$0" /mnt/setup/setup.sh
	
	echo "$ANSIBLE_VAULT_PASS" > /mnt/setup/.vault_pass.txt
	
	if [ "$ANSIBLE_NVIDIA" == "y" ]; then
		echo "arch_nvidia: True" >> /mnt/setup/ansible_vars
	fi	
	if [ "$ANSIBLE_NVIDIA_OPTIMUS" == "y" ]; then
		echo "arch_nvidia_optimus: True" >> /mnt/setup/ansible_vars
		echo "arch_nvidia_busid: "'"'"$ANSIBLE_NVIDIA_BUSID"'"' >> /mnt/setup/ansible_vars

	fi

	echo "arch_cryptdevice: $DISK_DEVICE"3 >> /mnt/setup/ansible_vars
	echo "arch_swapdevice: /dev/mapper/vg0-swap" >> /mnt/setup/ansible_vars
	echo "arch_hostname: $NEW_HOSTNAME" >> /mnt/setup/ansible_vars
	echo "arch_ansible_vars_path: /setup/ansible_vars" >> /mnt/setup/ansible_vars
	echo "create_repo: true" >> /mnt/setup/ansible_vars

	arch-chroot /mnt ./setup/setup.sh chroot

	if [ -f /mnt/setup/setup.sh ]
    	then
        	echo 'ERROR: Something failed inside the chroot, not unmounting filesystems so you can investigate.'
        	echo 'Make sure you unmount everything before you try to run this script again.'
    	#else
        #	echo 'Unmounting filesystems'
        #	umount -R /mnt
	#	swapoff -a
	#	cryptsetup luksClose luks
	#	echo 'Done! Reboot system.'
	fi
}

ansible() {
	EXTRA_VARS=""
	
	if [ -f /setup/ansible_vars ]; then
		EXTRA_VARS='--extra-vars @/setup/ansible_vars'
	fi
	export PYTHONUNBUFFERED=true
	ansible-pull -U https://gitlab.com/networkjanitor/ansible-xyoz-arch.git $EXTRA_VARS --track-subs --accept-host-key --clean -vv --vault-password-file=/setup/.vault_pass.txt playbook.yml
}

configure() {
	ansible	
	
	# Cleanup
	rm -rf /setup/
}



#################
set -ex

if [ "$1" == "chroot" ]
then
    configure
elif [ "$1" == "ansible" ]
then
    ansible
else
    setup
fi
#############


