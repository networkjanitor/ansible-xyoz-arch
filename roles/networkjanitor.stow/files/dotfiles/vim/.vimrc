if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" We UTF-8 now
set encoding=utf-8

" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar
nnoremap <space> za

" Python3 Settings
au BufNewFile,BufRead *.py
    \ set tabstop=4 softtabstop=4 expandtab autoindent |
    \ set shiftwidth=4 |
    \ set fileformat=unix |
    \ let python_highlight_all=1


" Autocomplete
let g:ale_completion_enabled = 1
Plug 'w0rp/ale'

" Fix on save
let g:ale_fix_on_save = 1

" ALE fixers
let g:ale_fixers = {
\   'python': ['yapf'],
\   'yaml': ['prettier'],
\   'rust': ['cargo', 'rls']
\}
" ALE linters
let g:ale_linters = {
\   'python': ['pyls'],
\}

let g:ale_python_flake8_options = '--max-line-length=120'

" Web Settings
au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2

" Syntax Checking/Highlighting
Plug 'pearofducks/ansible-vim'
syntax on
set nu

" NERDTree
Plug 'scrooloose/nerdtree'
let NERDTreeIgnore=['\.pyc$', '\~$'] " ignore files in NERDTree

" Possibility to start/close NERDTree on startup/shutdown, but it annoyed me.
"autocmd vimenter * NERDTree
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Git Integration
Plug 'tpope/vim-fugitive'


" Initialize plugin system
call plug#end()
