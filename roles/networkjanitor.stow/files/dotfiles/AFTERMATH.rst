Borg Restore
------------

.. code:
      # get credentials from keepass for borg-repositories
      # export creds, escape single quote sign like: 'This is a '\''-sign'
      export BORG_PASSSPHRASE='<creds>'
      borg list borg@backupsrv:/mnt/storage/borg-repositories/<reponame>
      # figure out the path construction inside the backup and to which location to restore it
      borg list borg@backupsrv:/mnt/storage/borg-repositories/<reponame>::<hostname>-<title>-<date>
      # cd to that directory
      cd /
      # and finally extract the data
      borg extract borg@backupsrv:/mnt/storage/borg-repositories/<reponame>::<hostname>-<title>-<date>
