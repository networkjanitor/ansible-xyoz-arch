Lutris
======

`pacman -Sy gnome-desktop zenity python-pillow webkit2gtk`

- gnome-desktop for GObjects
- zenity for winetricks gui
- python-pillow (PIL) and webkit2gtk for lutris 0.5.0 update

League of Legends
-----------------
Fix this (or any variation for xaudio2 dlls): 

`004e:err:module:load_builtin_dll failed to load .so lib for builtin L"xaudio2_0.dll": libopenal.so.1: cannot open shared object file: No such file or directory`

with this:

`pacman -Sy lib32-openal openal`
