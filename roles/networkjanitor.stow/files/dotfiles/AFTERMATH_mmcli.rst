ModemManager
============

List all modems: `mmcli -L`
Send USSD codes: `mmcli -m <modemid> --3gpp-ussd-initiate='*101#'`
Respond USSD codes: `mmcli -m <modemid> --3gpp-ussd-respond="1"`
Cancel USSD session: `mmcli -m <modemid> --3gpp-ussd-cancel`
USSD Status: `mmcli --3gpp-ussd-status`

