Smartcard
=========

https://wiki.archlinux.org/index.php/Smartcards

.. code:
        systemctl start pcscd.service
        # Tip: If you get the error Failed to start pcscd.service: Unit pcscd.socket not found., just reload systemd units with this 
        systemctl daemon-reload

        pcsc_scan 


Firefox
-------
The browser needs to set the new security-related device. Open the Security Devices page (reach it via Preferences, Privacy & Security, Certificates), then click Load and set the Module Name to CAC Module and module filename to /usr/lib/opensc-pkcs11.so.

Chromium
--------
Chromium uses NSS. Open a shell in your home directory and verify that the CAC Module is not already present:

.. code:
        $ modutil -list -dbdir .pki/nssdb/

         Listing of PKCS #11 Modules
        -----------------------------------------------------------
          1. NSS Internal PKCS #11 Module
            ....

If not, close any browser and add the module (an user interaction for confirmation is required):

.. code:
         $ modutil -dbdir sql:.pki/nssdb/ -add "CAC Module" -libfile /usr/lib/opensc-pkcs11.so

         WARNING: Performing this operation while the browser is running could cause
         corruption of your security databases. If the browser is currently running,
         you should exit browser before continuing this operation. Type
         'q <enter>' to abort, or <enter> to continue:

         Module "CAC Module" added to database.

Check for the correct execution of the command:

.. code:
         $ modutil -list -dbdir .pki/nssdb/

         Listing of PKCS #11 Modules
         -----------------------------------------------------------
           1. NSS Internal PKCS #11 Module
             ....
           2. CAC Module
             library name: /usr/lib/opensc-pkcs11.so
                uri: pkcs11:library-manufacturer=OpenSC%20Project;library-description=OpenSC%20smartcard%20framework;library-version=0.19
              slots: 1 slot attached
             status: loaded

