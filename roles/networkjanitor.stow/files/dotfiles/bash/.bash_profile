# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

export VISUAL=vim
export EDITOR=vim

# User specific environment and startup programs
if [ -d "$HOME/bin" ] ; then
    export PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
    export PATH="$PATH:$HOME/.local.bin"
fi

if [ -d "$HOME/bin/adb-fastboot/platform-tools" ] ; then
    export PATH="$HOME/bin/adb-fastboot/platform-tools:$PATH"
fi

if [ -d "$HOME/bin/go/bin" ] ; then
    export PATH="$HOME/bin/go/bin:$PATH"
fi

if [ -d "$HOME/go/bin" ] ; then
    export PATH="$HOME/go/bin:$PATH"
fi

export PATH
