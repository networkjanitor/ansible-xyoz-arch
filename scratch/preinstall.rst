# Create bootable USB

```bash
dd bs=4M if=/path/to/archlinux.iso of=/dev/sdx status=progress && sync
```

# Boot from USB and set prepare system

```bash
loadkeys <your-keymap>
timedatectl set-ntp true
```

# Connect to wifi

```bash
wifi-menu
```

# Partition the disk with gdisk

We will create 2 partitions, one for boot partition and one for LUKS encrypted partition

```bash
gdisk /dev/sda
```

```
GPT fdisk (gdisk) version 1.0.1

Partition table scan:
  MBR: protective
  BSD: not present
  APM: not present
  GPT: present

Found valid GPT with protective MBR; using GPT.

Command (? for help): o
This option deletes all partitions and creates a new protective MBR.
Proceed? (Y/N): Y

Command (? for help): n
Partition number (1-128, default 1): 
First sector (34-242187466, default = 2048) or {+-}size{KMGTP}: 
Last sector (2048-242187466, default = 242187466) or {+-}size{KMGTP}: +512M
Current type is 'Linux filesystem'
Hex code or GUID (L to show codes, Enter = 8300): EF00
Changed type of partition to 'EFI System'

Command (? for help): n
Partition number (2-128, default 2): 
First sector (34-242187466, default = 1050624) or {+-}size{KMGTP}: 
Last sector (1050624-242187466, default = 242187466) or {+-}size{KMGTP}: 
Current type is 'Linux filesystem'
Hex code or GUID (L to show codes, Enter = 8300): 
Changed type of partition to 'Linux filesystem'

Command (? for help): p
Disk /dev/sda: 242187500 sectors, 115.5 GiB
Logical sector size: 512 bytes
Disk identifier (GUID): 9FB9AC2C-8F29-41AE-8D61-21EA9E0B4C2A
Partition table holds up to 128 entries
First usable sector is 34, last usable sector is 242187466
Partitions will be aligned on 2048-sector boundaries
Total free space is 2014 sectors (1007.0 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048         1050623   512.0 MiB   EF00  EFI System
   2         1050624       242187466   115.0 GiB   8300  Linux filesystem

Command (? for help): w
```

# Format, encrypt and mount partitions

I will create only swap and root partitions, but here you can create home, var and other partitions if you wish.

```bash
mkfs.vfat -F32 /dev/sda1

cryptsetup -v luksFormat /dev/sda2
cryptsetup luksOpen /dev/sda2 luks

pvcreate /dev/mapper/luks
vgcreate vg0 /dev/mapper/luks
lvcreate -L 4G vg0 -n swap
lvcreate -l +100%FREE vg0 -n root

mkfs.ext4 /dev/mapper/vg0-root
mkswap /dev/mapper/vg0-swap

mount /dev/mapper/vg0-root /mnt
swapon /dev/mapper/vg0-swap

mkdir /mnt/boot
mount /dev/sdX1 /mnt/boot
```

# Install base system

```bash
pacstrap /mnt base base-devel
```

# Generate fstab

```bash
genfstab -pU /mnt >> /mnt/etc/fstab

cat /mnt/etc/fstab
# 
# /etc/fstab: static file system information
#
# <file system>	<dir>	<type>	<options>	<dump>	<pass>
# /dev/mapper/vg0-root
UUID=44bc2285-0443-44d6-8208-e914638ee1b1	/         	ext4      	rw,noatime,data=ordered	0 1

# /dev/sda1
UUID=AEF3-11A1      	/boot     	vfat      	rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro	0 2

# /dev/mapper/vg0-swap
UUID=708a05f7-633c-4f0f-a16b-3abce7def965	none      	swap      	defaults  	0 0

```
If you have SSD change relatime on all non-boot partitions to noatime.

# chroot into new system and prepare it

```bash
arch-chroot /mnt

ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc

echo <your-hostname> > /etc/hostname

pacman -S fish
pacman -S dialog wpa_supplicant iw linux-firmware wicd

passwd
useradd -m -G wheel -s /usr/bin/fish <username>
passwd <username>

```
# Set locales
Uncomment en_US.UTF-8 UTF-8 and other needed localizations in /etc/locale.gen
```bash
echo LANG=en_US.UTF-8 > /etc/locale.conf
echo KEYMAP=<your-keymap> > /etc/vconsole.conf
locale-gen
```

# mkinitcpio

```bash
bootctl --path=/boot install
```

Edit /etc/mkinitcpio.conf

```
MODULES="ext4"
.
.
.
HOOKS="base udev autodetect modconf block keymap encrypt lvm2 resume filesystems keyboard fsck"
```

```bash

```
# Configure bootloader

Create /boot/loader/entries/arch.conf

```
title	Arch Linux
linux	/vmlinuz-linux
initrd	/initramfs-linux.img
options cryptdevice=UUID=<YOUR-PARTITION-UUID>:lvm:allow-discards resume=/dev/mapper/vg0-swap root=/dev/mapper/vg0-root rw quiet
```

Edit /boot/loader/loader.conf

```
timeout 0
default arch
editor 0
```

# Finish installation and boot to new system

```bash
mkinitcpio -p linux
exit
umount -R /mnt
reboot
```