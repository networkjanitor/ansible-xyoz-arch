Parent Tutorial:
- https://gist.github.com/mattiaslundberg/8620837

Muktware Tutorial:
- http://www.muktware.io/arch-linux-guide-the-always-up-to-date-arch-linux-tutorial/

Arch Linux Installation Guide:
- https://wiki.archlinux.org/index.php/Installation_guide

There are 2 choices:
- UEFI/GPT mode: UEFI boot mode / GPT partition table
- BIOS/MBR mode: Legacy boot mode / MBR partition table

I tried to install in UEFI mode, but my laptop (Acer E5-475-336H) had problems with it, and didn't boot after installation.
This is how I installed arch linux in BIOS/MBR mode with full disk encryption (using LUKS), and LVM on LUKS.

# [IMPORTANT] ASSUMPTIONS

I assume that /dev/sda is the system's disk, and /dev/sdb is usb drive.


# STEPS

1.  Download arch iso image from https://www.archlinux.org/ and copy to a usb drive.
        
        # dd if=arch.iso of=/dev/sdb

2.  Set boot mode to "Legacy" in BIOS configuration, and boot from usb.

3.  Connect to internet. Useful commands:

        # supervisorctl restart dhcpcd
        # wifi-menu

4.  Partitioning

    A drive should first be **partitioned** and afterwards the partitions should be formatted with a **file system**.
    Use fdisk to create MBR partitions.
    
        # fdisk /dev/sda
    
    First, create an empty MBR partition table (WARNING: This will erase entire disk)
    
        (fdisk) o
    
    We are going to create 2 main partitions (/dev/sda1 and /dev/sda2):
    
        Device     Boot     Start       End   Sectors   Size Id Type
        /dev/sda1            2048    526335    524288   256M 83 Linux      /boot
        /dev/sda2          526336 765986815 765460480   365G 83 Linux      Encrypted with LUKS, 3 LVM partitions:
            swap  vg0 -wi-ao----   8.00g                                   swap
            root  vg0 -wi-ao----  80.00g                                   /
            anbar vg0 -wi-ao---- 277.00g
        /dev/sda3       765986816 976773167 210786352 100.5G 83 Linux      (Optional) Other partitions if you need... You can encrypt them separately with another password
    
    Create partitions:
    
        (fdisk) n
        (fdisk) p
        (fdisk) 1
        (fdisk) <Enter>
        (fdisk) +256M
        (fdisk) t
        (fdisk) 83
        
        (fdisk) n
        (fdisk) p
        (fdisk) 2
        (fdisk) <Enter>
        (fdisk) +365G
        (fdisk) t
        (fdisk) 83
        
        (fdisk) n
        (fdisk) p
        (fdisk) 3
        (fdisk) <Enter>
        (fdisk) <Enter>
        (fdisk) t
        (fdisk) 83
        
        (fdisk) w (Write Changes)

    Format Partitions:

        mkfs.ext2 /dev/sda1

5.  Setup encryption

        # cryptsetup -c aes-xts-plain64 -y --use-random luksFormat /dev/sda2
        # cryptsetup luksOpen /dev/sda2 luks

6.  Create LVM Partitions
    This creates one partions for root, modify if /home or other partitions should be on separate partitions
        
        # pvcreate /dev/mapper/luks
        # vgcreate vg0 /dev/mapper/luks
        # lvcreate --size 8G vg0 --name swap
        # lvcreate --size 80G vg0 --name root
        # lvcreate -l +100%FREE vg0 --name anbar

7.  Format LVM partitions

        # mkfs.ext4 /dev/mapper/vg0-root
        # mkfs.ext4 /dev/mapper/vg0-anbar
        # mkswap /dev/mapper/vg0-swap

8.  Mount the new system 

        # mount /dev/mapper/vg0-root /mnt
        # mkdir /mnt/boot
        # mount /dev/sda1 /mnt/boot
        # swapon /dev/mapper/vg0-swap

9.  Install the base system

        # pacstrap -i /mnt base base-devel openssh git vim

10. Generate /etc/fstab
    The /etc/fstab file can be used to define how disk partitions, various other block devices,
    or remote filesystems should be mounted into the filesystem. 

        # genfstab -pU /mnt >> /mnt/etc/fstab
    
    (Optional) For making /tmp a ramdisk, add following line to /mnt/etc/fstab:
    
        tmpfs	/tmp	tmpfs	defaults,noatime,mode=1777	0	0

11. Enter the new system

        # arch-chroot /mnt /bin/bash

12. Set TimeZone
    See available timezones: ls /usr/share/zoneinfo/
    
        # ln -s /usr/share/zoneinfo/Asia/Tehran /etc/localtime

13. Set Locale

        # vim /etc/locale.gen (uncomment en_US.UTF-8 UTF-8)
        # locale-gen
        # echo LANG=en_US.UTF-8 > /etc/locale.conf
		# export LANG=en_US.UTF-8

14. Set the hardware clock mode uniformly between your operating systems.
    Otherwise, they may overwrite the hardware clock and cause time shifts. 
		
        # hwclock --systohc --utc

15. Set hostname
		
        # echo myhostname >/etc/hostname
	
    Add it to /etc/hosts:
	
        127.0.1.1	myhostname.localdomain	myhostname

16. Create User

        # useradd -m -g users -G wheel -s myusername
        # passwd myusername
        # visudo
		uncomment %wheel ALL=(ALL) ALL

17. Configure mkinitcpio with modules needed for the initrd image

        # vim /etc/mkinitcpio.conf
        Add 'ext4' to MODULES
        Add 'encrypt' and 'lvm2' to HOOKS before 'filesystems'

    Regenerate initrd image

        # mkinitcpio -p linux

18. Setup grub
        
        # pacman -S grub
		# grub-install --target=i386-pc --recheck /dev/sda
        
    In /etc/default/grub edit the line GRUB_CMDLINE_LINUX to:
        
        GRUB_CMDLINE_LINUX="cryptdevice=/dev/sda2:luks:allow-discards"
		
    [Tip] To automatically search for other operating systems on your computer,
    install os-prober (pacman -S os-prober) before running the next command.
		
        # grub-mkconfig -o /boot/grub/grub.cfg

19. Exit new system and unmount all partitions

        # exit
        # umount -R /mnt
        # swapoff -a


20. Reboot into the new system, don't forget to remove the cd/usb
        
        # reboot

21. Start network and check internet

        # systemctl enable dhcpcd.service
        # systemctl start dhcpcd.service

22. System is installed now. If you want to install Gnome desktop, perform following steps.

23. Install gdm

        # pacman -S gdm
        # pacman -S xorg-server
        # pacman -S xorg-xinit
        # pacman -S xorg-server-utils
        # INSTALL GRAPHICS DRIVER (e.g. xf86-video-intel)
        # pacman -S xterm
        # startx
        # exit

24. Install Gnome

        # pacman -S gnome
        (Optional) # gnome-extra gnome-system-tools

25. Enable GDM service

        # systemctl enable gdm

26. Reboot!