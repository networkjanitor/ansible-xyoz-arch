VAR_FILE = group_vars/all/vars.yml
VAULT_FILE = group_vars/all/vault.yml
VAULT_PASS_FILE = .vault_pass.txt

EDITOR ?= vim

ifeq "$(ANSIBLE_VERBOSITY)" ""
    ANSIBLE_VERBOSITY = -vv
endif

submodules:
	git submodule update --recursive --remote

playbook:
	@ansible-playbook $(ANSIBLE_VERBOSITY) --vault-password-file=$(VAULT_PASS_FILE) $(ANSIBLE_OPTS) playbook.yml

apps:
	@ANSIBLE_OPTS='--tags=apps' make -e playbook

config:
	@ANSIBLE_OPTS='--tags=config' make -e playbook
retry:
	@ANSIBLE_OPTS='--limit @playbook.retry' make -e matrix

clean:
	rm -rf *.retry

edit:
	@if [ -r $(VAR_FILE) ]; then \
		$(EDITOR) $(VAR_FILE); \
	else \
	  	echo "no var file found at $(VAR_FILE)."; \
    	fi

edit-vault:
	@if [ -r $(VAULT_FILE) ]; then \
		trap "ansible-vault encrypt --vault-password-file $(VAULT_PASS_FILE) $(VAULT_FILE)" EXIT; \
		ansible-vault decrypt --vault-password-file $(VAULT_PASS_FILE) $(VAULT_FILE); \
		$(EDITOR) $(VAULT_FILE); \
	else \
		echo "no vault found at $(VAULT_FILE)."; \
    	fi
